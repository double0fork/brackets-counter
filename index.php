<?php
require 'src/BracketsCounter.php';



$shortOptions = 's:';
$longOptions = ['string:'];
$options = getopt($shortOptions, $longOptions);

$string = null;
$string = $options['s'] ?? $string;
$string = $options['string'] ?? $string;


if ($string) {
    echo (new Project\BracketsCounter($string))->count();

}

