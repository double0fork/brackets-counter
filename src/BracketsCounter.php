<?php
namespace Project;

class BracketsCounter
{
    /**
     * @var string|array|string[]
     */
    private string $expression;

    public function __construct($expression)
    {
        $this->expression = str_replace(' ', '',  $expression);
    }

    public function count(): string
    {
        $stack = [];
        $result = [];

        for ($i = 0, $length = strlen($this->expression); $i < $length; $i++) {
            $char = $this->expression[$i];

            if ($char == "(") {
                $stack[] = $i;
                continue;
            }

            if ($char == ")") {
                if (empty($stack)) {
                    throw new \InvalidArgumentException("Error with parse in position " . $i);
                }

                $position = array_pop($stack);

                $result[$position] = $i;
            }
        }

        if (!empty($stack)) {
            throw new \InvalidArgumentException("Error with parse in position " . array_pop($stack));
        }

        if (empty($result)) {
            throw new \InvalidArgumentException("Скобки отсутствуют ");
        }

        return $this->prepare($result);

    }

    private function prepare($result): string
    {
        ksort($result);

        $string = "All is OK" . PHP_EOL;
        foreach ($result as $at => $to) {
            $string .= '(' . $at . ', ' . $to . '), ';
        }
        return trim($string, ", ");
    }

}